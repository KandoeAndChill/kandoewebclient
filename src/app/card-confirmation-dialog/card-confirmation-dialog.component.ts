import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {CardDTO} from '../model/CardDTO';
import {DialogData} from '../DialogData';

@Component({
  selector: 'app-card-confirmation-dialog',
  templateUrl: './card-confirmation-dialog.component.html',
  styleUrls: ['./card-confirmation-dialog.component.css']
})
export class CardConfirmationDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<CardConfirmationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onYesClick(card: CardDTO): void {
    this.dialogRef.afterClosed();
    this.dialogRef.close();
  }

}
