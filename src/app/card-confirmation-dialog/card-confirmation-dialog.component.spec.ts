import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardConfirmationDialogComponent } from './card-confirmation-dialog.component';

describe('CardConfirmationDialogComponent', () => {
  let component: CardConfirmationDialogComponent;
  let fixture: ComponentFixture<CardConfirmationDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardConfirmationDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardConfirmationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
