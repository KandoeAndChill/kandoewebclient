import {
  AfterViewInit,
  ChangeDetectorRef,
  Component, DoCheck,
  ElementRef,
  HostListener,
  OnDestroy,
  OnInit,
  PipeTransform,
  ViewChild
} from '@angular/core';
import {SessionService} from '../service/session.service';
import {SessionDTO} from '../model/SessionDTO';
import {MatTableDataSource, MatSort, MatPaginator, MatChipInputEvent, MatTabGroup} from '@angular/material';
import {STATUS} from '../model/status';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {MdbTablePaginationComponent, MdbTableService} from 'angular-bootstrap-md';
import {MatTable} from '@angular/material';
import {Theme} from '../model/Theme';
import {st} from '@angular/core/src/render3';
import {SessionCreateDTO} from '../model/SessionCreateDTO';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import * as _moment from 'moment';
import {default as _rollupMoment} from 'moment';
import {BehaviorSubject} from 'rxjs';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {ThemeService} from '../service/theme.service';

const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-my-sessions',
  templateUrl: './my-sessions.component.html',
  styleUrls: ['./my-sessions.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})

export class MySessionsComponent implements OnInit, DoCheck {
  // @ViewChild(MdbTablePaginationComponent) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild('sessiontable') sessionTable: MatTable<any>;
  @ViewChild('sessiontable2') sessionTable2: MatTable<any>;
  @ViewChild('sessiontable3') sessionTable3: MatTable<any>;
  sessions: SessionDTO[] = [];
  sessions2: SessionDTO[] = [];
  sessions3: SessionDTO[] = [];
  themesFromUser: Theme[] = [];
  dataSource: MatTableDataSource<SessionDTO>;
  dataSource2: MatTableDataSource<SessionDTO>;
  dataSource3: MatTableDataSource<SessionDTO>;
  selectedTab: number;
  loading = false;
  // dataSource: BehaviorSubject<SessionDTO[]>;
  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild('paginator2') paginator2: MatPaginator;
  @ViewChild('paginator3') paginator3: MatPaginator;
  @ViewChild(MatTabGroup) tabGroup: MatTabGroup;
  @ViewChild('matSort1') sort: MatSort;
  @ViewChild('matSort2') sort2: MatSort;
  @ViewChild('matSort3') sort3: MatSort;
  displayedColumns: String[] = ['id', 'name', 'state', 'type', 'startTime', 'action'];
  // Session create form data
  time = {hour: 13, minute: 30};
  date: Date;
  sessionformsubmitted = false;
  sessionform: FormGroup;
  customSessionMixMaxCardsCreateError = false;
  customSessionStartDateRequiredError = false;
  private closeResult;
  roundtimelimit = false;
  usercardsenabled = false;
  specificstarttime = false;
  mindate;
  createdSession;
  theme: Theme = new Theme();
  themeName = '';
  fromTheme: boolean;
  //
  usermails: string[] = [];
  selectedSessionId = -1;
  userform: FormGroup;
  userformsubmitted = false;
  invalidmailformat = false;
  selectable = true;
  removable = true;
  addOnBlur = true;
  updated: boolean;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private sessionService: SessionService,
              private themeService: ThemeService,
              private formBuilder: FormBuilder,
              private modalService: NgbModal,
              private cdRef: ChangeDetectorRef,
              private tableService: MdbTableService,
              private changeDetectorRefs: ChangeDetectorRef) {
    // this.dataSource = new BehaviorSubject(this.sessions);
  }

  ngOnInit() {
    this.themeService.getThemesOfUser()
      .then((value: Theme[]) => {
        this.themesFromUser = this.themeService.results;
        this.getTheme();
      }, (err) => {
        console.log('Error fetching themes: ' + err);
      });
    this.updated = false;
  }

  ngDoCheck(): void {
    if (this.updated === false) {
    this.getSessions();
    }
  }

  getTheme() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      if (params.has('id')) {
        console.log('Parameter: ' + params.get('id'));
        this.fromTheme = true;
        // set theme
        this.theme = this.themesFromUser.filter(theme => theme.id === +params.get('id'))[0];
        // set tab to theme tab
        this._setDataSource(2);
      } else {
        this.fromTheme = false;
        this._setDataSource(0);
      }
    });
  }

  _setDataSource(indexNumber) {
    setTimeout(() => {
      switch (indexNumber) {
        case 0:
          this.selectedTab = indexNumber;
          this.getSessions();
          break;
        case 1:
          console.log('here');
          this.selectedTab = indexNumber;
          this.getSessions();
          break;
        case 2:
          this.selectedTab = indexNumber;
          this.getSessions();
      }
    });
  }

  getSessions() {
    this.loading = true;
    this.updated = true;
    switch (this.selectedTab) {
      case 0:
        this.sessions = [];
        this.sessionService.getSessionsForOrganiser().then(_ => {
          (this.loading = false);
          this.sessions = this.sessionService.results;
          // console.log('Sessions for organiser: ' + this.sessions);
          this.dataSource = new MatTableDataSource(this.sessions);
          this.cdRef.detectChanges();
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }, (err) => {
          console.log('Error fetching Sessions for Organiser: ' + err);
        });
        break;
      case 1:
        this.sessions2 = [];
        this.sessionService.getSessionsForUser().then(_ => {
          (this.loading = false);
          this.sessions2 = this.sessionService.results;
          console.log('Sessions for user: ' + this.sessions2);
          this.dataSource2 = new MatTableDataSource(this.sessions2);
          this.cdRef.detectChanges();
          this.dataSource2.paginator = this.paginator2;
          this.dataSource2.sort = this.sort2;
        }, (err) => {
          console.log('Error fetching Sessions for User: ' + err);
        });
        break;
      case 2:
        this.sessionService.getSessionsForTheme(this.theme.id).then(_ => {
          this.sessions3 = [];
          (this.loading = false);
          this.sessions3 = this.sessionService.results;
          console.log('Sessions for theme: ' + this.sessions3);
          this.dataSource3 = new MatTableDataSource(this.sessions3);
          this.cdRef.detectChanges();
          this.dataSource3.paginator = this.paginator3;
          this.dataSource3.sort = this.sort3;
        }, (err) => {
          console.log('Error fetching Sessions for Theme: ' + err);
        });
    }

  }

  convertChanceCircle(chance: boolean): string {
    if (chance) {
      return 'chance';
    } else {
      return 'problem';
    }
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    if (this.selectedTab === 0) {
      this.dataSource.filter = filterValue;
    } else if (this.selectedTab === 1) {
      this.dataSource2.filter = filterValue;
    } else if (this.selectedTab === 2) {
      this.dataSource3.filter = filterValue;
    }
  }

  // Session Create Form Logic
  openSessionModal(content) {
    this.initializeSessionForm();
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openUserModal(content, sessionId: number) {
    this.selectedSessionId = sessionId;
    this.initializeUserInviteForm();
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  private onNewSessionSubmit(modal) {
    this.sessionformsubmitted = true;
    // console.log(this.date);
    this.customSessionMixMaxCardsCreateError = ((this.sessionform.get('maxcardsnr').value
      <= this.sessionform.get('mincardsnr').value));
    this.customSessionStartDateRequiredError = (!this.date && this.specificstarttime ? true : false);

    // stop here if form is invalid
    if (this.sessionform.invalid || this.customSessionMixMaxCardsCreateError || this.customSessionStartDateRequiredError) {
      return;
    }
    modal.close();
    const sessionToCreate = new SessionDTO();
    sessionToCreate.themeId = this.sessionform.get('theme').value;
    sessionToCreate.name = this.sessionform.get('name').value;
    sessionToCreate.chanceCircle = !(this.sessionform.get('circletype').value.toString().toLowerCase() === 'problem');
    sessionToCreate.numberOfCircles = this.sessionform.get('nrofcircles').value;
    sessionToCreate.numberOfRounds = this.sessionform.get('nrofrounds').value;

    sessionToCreate.maxCards = this.sessionform.get('maxcardsnr').value;
    sessionToCreate.minCards = this.sessionform.get('mincardsnr').value;
    sessionToCreate.userCardsEnabled = true;

    if (this.specificstarttime) {
      let starttime: Date;
      starttime = new Date(this.date.toUTCString());
      starttime.setHours(this.time.hour + 1); // Compensate for GMT time
      starttime.setMinutes(this.time.minute);
      sessionToCreate.startTime = starttime;
    }

    if (this.roundtimelimit) {

      sessionToCreate.roundTimeInMinutes = this.sessionform.get('rounddurationminutes').value;
    }
    console.log(sessionToCreate);
    // console.log(this.date);
    sessionToCreate.type = this.getSessionType(sessionToCreate);
    sessionToCreate.state = this.getSessionState(sessionToCreate);
    this.sessionService.createSession(sessionToCreate)
      .subscribe((value: SessionDTO) => {
        console.log(value);
        this.getSessions();
      });
  }

  getSessionState(session: SessionDTO) {
    return (new Date(session.startTime)).getTime() <= (new Date()).getTime() ? STATUS.OPEN : STATUS.PLANNED;
  }

  getSessionType(session: SessionDTO): string {
    console.log(session.chanceCircle);
    return session.chanceCircle ? 'chance' : 'problem';
  }

  get sessionformcontrols() {
    return this.sessionform.controls;
  }

  get userformcontrols() {
    return this.userform.controls;
  }

  initializeUserInviteForm() {
    this.userformsubmitted = false;
    this.invalidmailformat = false;
    this.usermails = [];
    this.userform = this.formBuilder.group({
      usermails: ['', Validators.email]
    });
  }

  initializeSessionForm() {
    this.sessionformsubmitted = false;
    this.customSessionMixMaxCardsCreateError = false;
    this.usercardsenabled = false;
    this.roundtimelimit = false;
    this.specificstarttime = false;
    this.customSessionStartDateRequiredError = false;
    this.date = null;
    this.sessionform = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      theme: new FormControl('', Validators.compose([Validators.required])),
      nrofcircles: [5, Validators.compose([Validators.required, Validators.min(1), Validators.max(8)])],
      nrofrounds: [10, Validators.compose([Validators.required, Validators.min(1)])],
      rounddurationminutes: [1, Validators.compose([Validators.required, Validators.min(1)])],
      mincardsnr: [1, Validators.compose([Validators.required, Validators.min(1), Validators.max(11)])],
      maxcardsnr: [2, Validators.compose([Validators.required, Validators.min(2), Validators.max(12)])],
      circletype: ['problem', Validators.compose([Validators.required])],
    });
  }

  disableNumberInputEdit(event) {
    event.preventDefault();
  }

  toggletimelimit() {
    this.roundtimelimit = !this.roundtimelimit;
  }

  togglespecificstarttime() {
    this.specificstarttime = !this.specificstarttime;
    this.customSessionStartDateRequiredError = !this.specificstarttime ? false : this.customSessionMixMaxCardsCreateError;
  }

  change(dateEvent) {
    // console.log(dateEvent);
    this.date = dateEvent.value ? dateEvent.value.toDate() : null;
    // console.log(this.date);
  }

  // Tag array functionality
  addTag(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.usermails.push(value);
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
// filter out duplicates
    const tempMailSet: Set<string> = new Set(this.usermails);
    this.usermails = Array.from(tempMailSet);
    this.onUserAddedRemoved();
  }

  removeTag(tag: string): void {
    const index = this.usermails.indexOf(tag);

    if (index >= 0) {
      this.usermails.splice(index, 1);
    }
    this.onUserAddedRemoved();
  }

  onUserListSubmit(modal) {
    this.userformsubmitted = true;
    if (this.userform.invalid || this.invalidmailformat) {
      return;
    }
    this.sessionService.inviteToSession(this.selectedSessionId, this.usermails).subscribe(session => {
      // this.getSessions();
    });
    this.selectedSessionId = -1;
    modal.close();
  }

  private onUserAddedRemoved() {
    const mailregex = '[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}';
    for (let i = 0; i < this.usermails.length; i++) {
      if (!this.usermails[i].match(mailregex)) {
        this.invalidmailformat = true;
        return;
      }
    }
    this.invalidmailformat = false;
    console.log(this.invalidmailformat);
  }

  deleteSessionClick(session: SessionDTO) {
    this.sessionService.deleteSession(session.id)
      .subscribe((value) => {
        this.updated = false;
      });
  }

  statusOpen(session: SessionDTO): boolean {
    return (session.state === 'Active');
  }

  statusClosed(session: SessionDTO): boolean {
    return (session.state === 'Passed');
  }

  joinSession(sessionId: number) {
    console.log('Redirect');
    this.router.navigate(['session/', sessionId]);
  }

  historySession(sessionId: number) {
    // redirect to history of this session
  }
}
