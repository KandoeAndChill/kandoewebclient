import {CardSessionDTO} from './CardSessionDTO';

export class GameSessionDTO {

  id: number;
  name: string;
  organiserId: number;
  gameOver: boolean;
  activeUserId: number;
  cardList: CardSessionDTO[];
  numberOfRounds;
}
