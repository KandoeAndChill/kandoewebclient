﻿export class User {
  private email: string;
  private username: string;
  private password: string;
  public avatarBase64: string;


  setAvatar(avatarBase64: string) {
    this.avatarBase64 = avatarBase64;
  }

  getEmail(): string {
    return this.email;
  }

  getUsername(): string {
    return this.username;
  }

  getPassword(): string {
    return this.password;
  }

  getAvatarBase64(): string {
    return this.avatarBase64;
  }

  setEmail(email: string) {
    this.email = email;
  }

  setUserName(userName: string) {
    this.username = userName;
  }

  setPassword(password: string) {
    this.password = password;
  }
}
