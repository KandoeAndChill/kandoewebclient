import {Theme} from './Theme';

export class CardDTO {
  id: number;
  imageBase64Url: string;
  name: string;
  themeId: number;

  setThemeId(id: number) {
    this.themeId = id;
  }

  setName(cardname: string) {
    this.name = cardname;
  }

  setImageUrl(imgbase64url: string) {
    this.imageBase64Url = imgbase64url;
  }

  setId(id: number) {
    this.id = id;
  }

  getId(): number {
    return this.id;
  }
}
