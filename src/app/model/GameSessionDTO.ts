import {CardSessionDTO} from './CardSessionDTO';

export class GameSessionDTO {

  id: number;
  name: string;
  themeId: number;
  organiserId: number;
  gameOver: boolean;
  activeUserId: number;
  chanceCircle: boolean;
  numberOfCircles: number;
  cardList: CardSessionDTO[];
  numberOfRounds: number;
  userCardsEnabled: boolean;
  cardsSelected: boolean;
}
