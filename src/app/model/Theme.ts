export class Theme {
  id: number;
  name: string;
  description: string;
  tag: string[];
  // userId: number;

  setName(name: string): void {
    this.name = name;
  }

  setDescription(description: string): void {
    this.description = description;
  }

  setTag(tag: string[]) {
    this.tag = tag;
  }

  getId(): number {
    return this.id;
  }

  setId(id: number) {
    this.id = id;
  }

  /*getuserId(): number {
    return this.userId;
  }

  setuserId(id: number) {
    this.userId = id;
  }*/
}
