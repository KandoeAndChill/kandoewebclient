export enum STATUS {
  CLOSED = 'CLOSED',
  OPEN = 'OPEN',
  ACTIVE = 'ACTIVE',
  PLANNED = 'PLANNED'
}
