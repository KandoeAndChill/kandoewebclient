import {STATUS} from './status';

export class SessionCreateDTO {
  name: string;
  // usermails: string[]; receive array of strings ? --> convert to 1 string
  numberOfCircles: number;
  numberOfRounds: number;
  minCards: number;
  maxCards: number;
  themeId: number;
  startTime: Date;
  userCardsEnabled: boolean;
  chanceCircle: boolean;
  roundTimeInMinutes: number;

  // TODO add more?
}
