import {jsonIgnore} from 'json-ignore';

export class SessionDTO {
  id: number;
  name: string;
  state: string;
  chanceCircle: boolean;
  themeId: number;
  startTime: Date;
  userCardsEnabled: boolean;
  numberOfCircles: number;
  numberOfRounds: number;
  roundTimeInMinutes: number;
  maxCards: number;
  minCards: number;

  // Calculated field based on chanceCircle boolean, used for mysessions table.
  @jsonIgnore()
  type: string;


}
