
// Both Request and Response.
export class CardSessionDTO {

  // Empty on create.
  id: number;

  cardText: string;
  imageUrl: string;
  sessionId: number;
  priority: number;



}
