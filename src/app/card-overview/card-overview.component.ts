import {Component, OnInit} from '@angular/core';
import {CardDTO} from '../model/CardDTO';
import { CardService } from '../service/card.service';
import { ThemeService } from '../service/theme.service';
import {GoogleLoginProvider} from 'angular-6-social-login';
import {ActivatedRoute, ParamMap} from '@angular/router';


@Component({
  selector: 'app-card-overview',
  templateUrl: './card-overview.component.html',
  styleUrls: ['./card-overview.component.css']
})
export class CardOverviewComponent implements OnInit {


  public cardCreationError: string;
  public activeCardCreationResponse = false;

  private cards: CardDTO[] = [];
  private themeId: number;

  constructor(private cardService: CardService, private themeService: ThemeService, private route: ActivatedRoute ) {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.themeId = +params.get('id');
    });
    this.getPostedCards();
  }

  ngOnInit() {
  }

  getPostedCards() {
    this.cardService.getCardsForTheme(this.themeId).subscribe(cards => this.addCards(cards));
  }

  public getCards(): CardDTO[] {
    return this.cards;
  }

  public onCardCreated(card: CardDTO) {

    this.activeCardCreationResponse = true;
    this.cards.push(card);
    setTimeout(() => {
      this.activeCardCreationResponse = false;
    }, 2000);
  }

  public onCardCreatedError(errormessage: string) {
    this.activeCardCreationResponse = true;
    this.cardCreationError = errormessage;
    setTimeout(() => {
      this.activeCardCreationResponse = false;
      this.cardCreationError = '';
    }, 2000);


  }

  addCard(card: CardDTO) {
    this.cards.push(card);
  }

  addCards(cards: CardDTO[]) {
    for (let i = 0; i < cards.length; i++) {
      this.cards.push(cards[i]);
    }
  }// Eventueel nog refreshen

  public fadeOutStatusMessage() {


  }

}
