import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatInputModule} from '@angular/material/input';
import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {routing} from './app-routing.module';
import {
  MatPaginatorModule,
  MatSortModule,
  MatToolbarModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatTableModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatChipsModule,
  MatSnackBarModule,
  MatTabsModule
} from '@angular/material';
import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider, LinkedinLoginProvider,
} from 'angular-6-social-login';
import {MatSelectModule} from '@angular/material/select';
import {CardCreatorComponent} from './card-view/card-creator.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {LayoutModule} from '@angular/cdk/layout';
import {AppRoutingModule} from './app-routing.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';
import {CardOverviewComponent} from './card-overview/card-overview.component';
import {CardService} from './service/card.service';
import {RegisterComponent} from './register/register.component';
import {UserService} from './service/user.service';
import {AlertService} from './service/alert.service';
import {AlertComponent} from './_directives';
import {LoginComponent} from './login/login.component';
import {ErrorInterceptor} from './helpers/error.interceptor';
import {SessionComponent} from './session/session.component';
import {AngularResizedEventModule} from 'angular-resize-event';
import {CardConfirmationDialogComponent} from './card-confirmation-dialog/card-confirmation-dialog.component';
import {CardSelectionComponent, RatingModalComponent} from './card-selection/card-selection.component';
import {NavBarComponent} from './nav-bar/nav-bar.component';
import {ThemeOverviewComponent} from './theme-overview/theme-overview.component';
import {ChatComponent} from './chat/chat.component';
import {ChatFormComponent} from './chat-form/chat-form.component';
import {MySessionsComponent} from './my-sessions/my-sessions.component';
import {ThemaViewComponent} from './theme-view/thema-view.component';
import {ChatService} from './service/chat.service';
import {WebsocketService} from './service/websocket.service';
import {MatCardModule} from '@angular/material';
import {MatButtonModule} from '@angular/material';
import {MatDialogModule} from '@angular/material';
import {MyDialogComponent} from './my-dialog/my-dialog.component';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
import {JwtInterceptor} from './helpers/jwtinterceptor';
import {MatMenuModule} from '@angular/material';
import {HomePageComponent} from './home-page/home-page.component';
import {CardratingComponent} from './cardrating/cardrating.component';
import {ModalModule} from 'ngx-bootstrap';
import {ThemeEditComponent} from './theme-edit/theme-edit.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import {EnvServiceProvider} from './service/env.service.provider';

// Configs

export function getAuthServiceConfigs() {
  const config = new AuthServiceConfig(
    [
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider('678771813600-hodbm849hql1p10uo5s09am6tbbetjf1.apps.googleusercontent.com')
      },

    ]
  );
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    CardCreatorComponent,
    CardOverviewComponent,
    RegisterComponent,
    AlertComponent,
    RatingModalComponent,
    LoginComponent,
    SessionComponent,
    CardConfirmationDialogComponent,
    CardSelectionComponent,
    NavBarComponent,
    ThemeOverviewComponent,
    MySessionsComponent,
    ThemeOverviewComponent,
    ChatComponent,
    ChatFormComponent,
    MySessionsComponent,
    ThemaViewComponent,
    MyDialogComponent,
    HomePageComponent,
    CardratingComponent,
    ThemeEditComponent,
    UserEditComponent,
  ],
  entryComponents: [
    RatingModalComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    MatInputModule,
    MatCardModule, MatButtonModule, FormsModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule,
    MatSortModule,
    HttpClientModule,
    LayoutModule,
    MatToolbarModule,
    MatChipsModule,
    MatTableModule,
    MatSidenavModule,
    ModalModule.forRoot(),
    MatDialogModule,
    MatIconModule,
    MatListModule,
    AppRoutingModule,
    NgbModule,
    ReactiveFormsModule,
    AngularResizedEventModule,
    MatTabsModule,
    routing,
    MatCardModule,
    MatMenuModule,
    MatButtonModule,
    MatDialogModule,
    MDBBootstrapModule,
    SocialLoginModule,
    MatSelectModule,
  ],



  exports: [MatFormFieldModule],
  providers: [EnvServiceProvider, CardService, UserService, AlertService, {
    provide: HTTP_INTERCEPTORS,
    useClass: ErrorInterceptor,
    multi: true
  },
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    { provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true},
    WebsocketService,
    ChatService],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
  ]
})
export class AppModule {
}
