import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {UserService} from '../service/user.service';
import {AlertService} from '../service/alert.service';
import {User} from '../model/User';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  loading = false;
  submitted = false;


  imageSrc: string | undefined;
  email: string;
  userName: string;
  password: string;
  newUser: User;

  @ViewChild('fileInput')
  imageInputField: ElementRef;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService,
    private alertService: AlertService
  ) {
  }


  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      username: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      image: new FormControl()
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  get user(): any {

    return localStorage.getItem('User');
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.newUser = new User();
    this.newUser.setEmail(this.email);
    this.newUser.setPassword(this.password);
    this.newUser.setUserName(this.userName);

    if (this.imageSrc !== undefined) {
      this.newUser.setAvatar(this.imageSrc);
      console.log(this.newUser.getAvatarBase64());
    }
    this.loading = true;
    this.userService.register(this.newUser).subscribe(
      data => {
        this.alertService.success('Registration successful', true);
        this.router.navigate(['/login']);
      },
      error => {
        if (error instanceof HttpErrorResponse) {
          this.alertService.error(error.message);
        }

        this.alertService.error(error);
        this.loading = false;
      });
  }
  fileChange(files: File[]) {
    if (files.length > 0) {
      this.upload(files[0]);
    }
  }
  upload(file: File) {
    const reader = new FileReader();
    console.log('upload');
    reader.onload = this._handleReaderLoaded.bind(this);
    const imageSrc = reader.readAsDataURL(file);
  }
  _handleReaderLoaded(e) {

    const reader = e.target;
    this.imageSrc = reader.result;
  }
  openInput() {
    this.imageInputField.nativeElement.click();
  }
}
