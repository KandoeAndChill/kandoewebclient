import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from '../model/User';
import {HttpErrorResponse} from '@angular/common/http';
import {AlertService} from '../service/alert.service';
import {UserService} from '../service/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {

  editForm: FormGroup;

  imageSrc: string;
  email: string;
  userName: string;
  password: string;

  @ViewChild('fileInput')
  imageInputField: ElementRef;
  newUser: User;

  constructor( private formBuilder: FormBuilder, private alertService: AlertService,
                private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.editForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      username: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      image: new FormControl()
    });
  }
  get f() {
    return this.editForm.controls;
  }

  get user(): any {

    return localStorage.getItem('User');
  }

  onSubmit() {
    // stop here if form is invalid
    if (this.editForm.invalid) {
      return;
    }

    this.newUser = new User();
    this.newUser.setEmail(this.email);
    this.newUser.setPassword(this.password);
    this.newUser.setUserName(this.userName);

    if (this.imageSrc !== undefined) {
      this.newUser.setAvatar(this.imageSrc);
      console.log(this.newUser.getAvatarBase64());
    }
    this.userService.updateUser(this.newUser).subscribe(
      data => {
        this.alertService.success('Changes successful', true);
        this.userService.logout();
        this.userService.login(this.newUser.getUsername(), this.newUser.getPassword());
        // Wait for the JWT to update
        setTimeout(() => {
            this.userService.getAuthenticatedUser().subscribe((user) => this.userService.setUserAvatar(user));
          },
          3000);
        this.router.navigateByUrl('');
      },
      error => {
        if (error instanceof HttpErrorResponse) {

        }
        const errorMessage = JSON.stringify(error);
        this.alertService.error(error.message);
      });
  }
  fileChange(files: File[]) {
    if (files.length > 0) {
      this.upload(files[0]);
    }
  }
  upload(file: File) {
    const reader = new FileReader();
    console.log('upload');
    reader.onload = this._handleReaderLoaded.bind(this);
    const imageSrc = reader.readAsDataURL(file);
  }
  _handleReaderLoaded(e) {

    const reader = e.target;
    this.imageSrc = reader.result;

  }
  openInput() {
    this.imageInputField.nativeElement.click();
  }

}
