import {
  AfterViewInit,
  Component,
  DoCheck,
  ElementRef,
  NgModule,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {CardService} from '../service/card.service';
import {getMockCards} from '../mockCards';
import {CardDTO} from '../model/CardDTO';
import {MatDialog, MatSnackBar, MatSnackBarConfig} from '@angular/material';
import {ThemeService} from '../service/theme.service';
import {UserService} from '../service/user.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {BsModalService, BsModalRef, ModalDirective} from 'ngx-bootstrap/modal';
import {Card} from '../model/Card';
import {CardRatingDTO} from '../model/CardRatingDTO';
import {Rating} from '../model/Rating';
import {Observable, Subscription} from 'rxjs';
import {SessionService} from '../service/session.service';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {SessionDTO} from '../model/SessionDTO';
import {switchMap} from 'rxjs/operators';
import {CardSessionDTO} from '../model/CardSessionDTO';
import {GameSessionDTO} from '../model/GameSessionDTO';
import {HttpErrorResponse} from '@angular/common/http';

// import {CardSessionService} from '../service/card-session.service';

@Component({
  selector: 'app-card-selection',
  templateUrl: './card-selection.component.html',
  styleUrls: ['./card-selection.component.css'],

})

export class CardSelectionComponent implements OnInit, OnDestroy {


  bsModalRef: BsModalRef;

  breakpoint: number;
  ThemeCards: Card[] = [];
  CustomCards: Card[] = [];


  selectedThemeCard: Card;
  selectedCustomCard: Card;

  currentUserThemeOrganiser: boolean;
  currentUserSessionOrganiser: boolean;

  @ViewChild('themecardratingmodal') themeCardRater: RatingModalComponent;

  session: GameSessionDTO;
  selectedCards: Card[] = [];

  currentSessionCards: CardSessionDTO[] = [];

  public cardCreationError: string;

  public activeCardCreationResponse = false;

  closeResult: string;

  activeUserName: String;

  sessionsubscription: Subscription;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private modalService: BsModalService,
              private userService: UserService,
              private cardService: CardService,
              private sessionService: SessionService,
              private snackBar: MatSnackBar) {

    // add theme cards
  }


  setThemeOrganiser(isorganiser: boolean) {

    this.currentUserThemeOrganiser = isorganiser;

  }

  setSessionOrganiser(isorganiser: boolean) {
    this.currentUserSessionOrganiser = isorganiser;
  }


  ngOnInit() {
    // this.ThemeCards = this.cardService.getCardsForSession(this.session);


    this.getCurrentSession();


    // this.getSessionCards();


  }

  skipTurn() {


    this.sessionService.skipTurn(this.session.id).subscribe(session => {

        this.session = session;

      },
      error => {

        // Min. amount of cards not selected!!
        if (error.status) {
          // Take particular action.

          this.openErrorSnackBar(error.message, '');

        }


      }
    );

  }

  isThemeCardSessionCard(sessionCard: CardSessionDTO): boolean {


    let found = false;


    const allThemeCards = this.ThemeCards;

    // allThemeCards.push(this.selectedThemeCard);


    for (let i = 0; i < allThemeCards.length; i++) {
      if (allThemeCards[i].name === sessionCard.cardText) {
        found = true;
        break;
      }
    }

    return found;

  }


  getCurrentSession() {

    let sessionId: number;

    this.route.paramMap.subscribe(params => {
      sessionId = parseInt(params.get('sessionid'), 10);
    });


    this.sessionService.getGameSession(sessionId).subscribe(session => this.setCurrentSession(session));


    // currentSession = session


  }




  setCurrentSession(session: GameSessionDTO) {

    this.session = session;

    console.log(this.session);


    this.userService.getUsernameById(session.activeUserId).subscribe(username => this.activeUserName = username);


    console.log(this.activeUserName);

    // Get associated data..
    // All current cards in session.
    this.currentSessionCards = this.session.cardList;

    // Available theme cards to select
    this.cardService.getAllCardsByTheme(this.session.themeId).subscribe(cards => this.ThemeCards = cards);


    this.userService.isOrganiserOfTheme(this.session.themeId).subscribe(isorganiser => this.setThemeOrganiser(isorganiser));

    this.userService.isOrganiserOfSession(this.session.id).subscribe(isorganiser => this.setSessionOrganiser(isorganiser));


    if (!this.sessionsubscription) {
      this.subscribeToSessionUpdate();
    }


  }

  getSessionCards() {

    this.sessionService.getAllSessionCardsBySessionId(this.session.id).subscribe(sessioncards => this.currentSessionCards = sessioncards);

  }


  // when user right-clicks on theme card.
  openThemeCardRatingModal(cardId: number, event) {

    // disable right click default menu
    event.preventDefault();


    const initialState = {

      data: cardId,


    };
    this.bsModalRef = this.modalService.show(RatingModalComponent, Object.assign({}, {
      class: 'modal-lg',
      initialState
    }));


  }

  startSessionPlayPhase() {

    this.sessionService.endSelectionPhaseSession(this.session.id).subscribe(gameSession => {
        this.redirectToSessionGameView(gameSession.id);
      },

      error => {


        if (error.status) {

          this.openErrorSnackBar(error.message, '');
        } else {

          // Expecting formatted message from error interceptor.
          this.openErrorSnackBar(error, '');


        }


      }
    );


  }

  redirectToSessionGameView(sessionId: number) {

    this.router.navigate(['/session', sessionId]);

  }


  refreshSession() {

    this.sessionService.getGameSession(this.session.id).subscribe(session => this.setCurrentSession(session));

    // this.cardService.getAllCardsByTheme(this.session.themeId).subscribe(cards => this.ThemeCards = cards);

  }

  // Can be used for creating multiple cards at once. Currently only 1 per turn
  submitSelectedCard() {

    let cardToBeCreated: Card;

    if (this.selectedCustomCard) {


      cardToBeCreated = this.selectedCustomCard;

    }

    if (this.selectedThemeCard) {


      cardToBeCreated = this.selectedThemeCard;
    }

    if (cardToBeCreated) {

      this.sessionService.selectCardForSession(cardToBeCreated, this.session.id)
        .subscribe(session => {
            this.setCurrentSession(session);
            console.log(session);
            this.sendSessionBroadCast(session);
          },
          error => {

            if (error.status) {
              // Take particular action.


              this.openErrorSnackBar(error.message, '');

              if (error.status === 460) {
                this.refreshSession();

              }

            } else {

              // Formatted error
              this.openErrorSnackBar(error, '');


            }


          });

      this.sendSessionBroadCast(this.session);

      this.resetCards();

    }

  }


  resetCards(): void {

    // this.ThemeCards.concat(this.selectedThemeCards); Not needed because of db refresh.
    this.selectedThemeCard = null;

    this.selectedCustomCard = null;


  }

  ngOnDestroy() {

    this.sessionsubscription.unsubscribe();

  }


  onThemeCardClickEvent(card: Card): void {

    if (this.selectedThemeCard || this.selectedCustomCard) {

      this.openErrorSnackBar('Currently only 1 card per turn can be selected!', '');

    } else {

        const i = this.ThemeCards.indexOf(card);
        this.selectedThemeCard = card;
        this.ThemeCards.splice(i, 1);


    }

  }


  sendSessionBroadCast(session: GameSessionDTO) {

    this.sessionService.broadcastSession(session);


  }

  subscribeToSessionUpdate() {

    this.sessionsubscription = this.sessionService.subscribeToSessionUpdate(this.session.id).
    subscribe(session => this.setCurrentSession(JSON.parse(session)));


  }


  onCardCreated(card: Card) {

    this.activeCardCreationResponse = true;
    this.CustomCards.push(card);
    this.openCardCreatedSnackBar('card created!', '');

  }


  addThemeCards(cards: Card[]) {

    this.ThemeCards = this.ThemeCards.concat(cards);

  }

  onCustomCardClickEvent(customCard: Card) {

    const i = this.CustomCards.indexOf(customCard);

    this.selectedCustomCard = customCard;
    this.CustomCards.splice(i, 1);

  }

  onClickSelectedThemeCard() {

    this.ThemeCards.push(this.selectedThemeCard);
    this.selectedThemeCard = null;

  }

  onClickSelectedCustomCard() {

    this.CustomCards.push(this.selectedCustomCard);
    this.selectedCustomCard = null;


  }


  openErrorSnackBar(message: string, action: string) {

    this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: ['error-snackbar']
    });

  }

  openCardCreatedSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
      panelClass: ['blue-snackbar']
    });
  }

  openSessionCardAlreadyExists(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: ['error-snackbar']
    });
  }


}


@Component({
  selector: 'app-rating-modal-content',
  styleUrls: ['./card-rating.component.css'],
  encapsulation: ViewEncapsulation.None,
  template: `
    <button mat-raised-button color="primary" #refreshbutton id='refreshbutton' (click)="this.refresh()"
            style="">Show all reviews!<span style=""><i
      class="fa fa-refresh" id="refreshicon"
      aria-hidden="true"></i></span></button>


    <div id="commentcontainer">
      <div class="modal-header">
        <h4 class="modal-title pull-left">Leave a comment...</h4>
        <button type="button" class="close pull-right" aria-label="Close" (click)="this.hideChildModal()">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>


      <div style="min-width: 500px " class="comment_block modal-body">

        <div id="commenterror" class="text-danger" *ngIf="this.errormessage">{{this.errormessage}}</div>


        <!-- used by #{user} to create a new comment -->
        <div class="create_new_comment">

          <!-- current #{user} avatar -->
          <div (click)="this.onCommentPosted()" class="user_avatar">
            <img src="https://s3.amazonaws.com/uifaces/faces/twitter/BillSKenney/73.jpg">
          </div><!-- the input field -->
          <div class="input_comment">
            <input [(ngModel)]="comment" (keydown)="onCommentPosted($event)" type="text" placeholder="Join the conversation..">
          </div>

        </div>


        <!-- new comment -->
        <div style="height: 400px;overflow: auto;" class="new_comment">
          <!-- build comment -->
          <ul class="user_comment">


            <div *ngFor="let rating of allRatings">
              <!-- current #{user} avatar -->
              <div class="user_avatar">
                <img src="https://s3.amazonaws.com/uifaces/faces/twitter/dancounsell/73.jpg">
              </div><!-- the comment body -->
              <div class="comment_body">
                <p>{{rating.comment}}</p>
              </div>

              <!-- comments toolbar -->
              <div class="comment_toolbar">

                <!-- inc. date and time -->
                <div class="comment_details">
                  <ul>
                    <li><i class="fa fa-clock-o"></i> {{getDateNow() | date:'H:mm' }}</li>
                    <li><i class="fa fa-calendar"></i> {{getDateNow() | date:'dd/MMM/yyyy' }}</li>
                    <li><i class="fa fa-pencil"></i> <span class="user">{{rating.username}}</span></li>
                  </ul>
                </div><!-- inc. share/reply and love -->
              </div>
            </div>


          </ul>

        </div>

      </div>

    </div>

  `
})

export class RatingModalComponent implements AfterViewInit, OnInit, DoCheck {


  comment: String;
  ratings: Rating[] = [];
  modalinitialized = false;
  errormessage: string;

  @ViewChild('childModal') childModal: ModalDirective;


  constructor(public bsModalRef: BsModalRef, private cardService: CardService) {


  }

  get allRatings(): Rating[] {
    return this.ratings.reverse();
  }


  showChildModal(): void {
    this.childModal.show();
  }

  hideChildModal(): void {

    this.bsModalRef.hide();

  }


  // Necessary workaround for initial rating refresh., this.bsModalRef.content.data is 'undefined' if accessed.
  ngDoCheck() {

    if (this.bsModalRef.content && !this.modalinitialized) {

      this.refresh();
      this.modalinitialized = true;
    }


  }


  onCommentPosted(event) {

    // When user presses enter.
    if (event.keyCode === 13) {

      const cardrating: CardRatingDTO = new CardRatingDTO();

      cardrating.cardId = this.bsModalRef.content.data;  // cardId was passed to this modalcomponent through config, field 'data'.

      cardrating.comment = this.comment;


      // api call automatically returns all ratings for card...
      this.cardService.postCardRating(cardrating).subscribe(ratings => {
          this.ratings = ratings;
        },
        error => {
          this.errormessage = error;
          setTimeout(() => {
            this.errormessage = '';
          }, 1000);
        }
      )
      ;

      this.comment = '';


    }


  }

  addRatings(ratings: Rating[]) {


    ratings.forEach(ratingcard => this.ratings.push(ratingcard));

  }

  getDateNow(): Date {

    return new Date();

  }

  refresh() {

    this.cardService.getAllCardRatings(this.bsModalRef.content.data).subscribe(ratings => this.ratings = ratings);

    // ratings.forEach(ratingcard => this.ratings.push(ratingcard))
  }


  ngOnInit() {


  }

  ngAfterViewInit(): void {


  }
}



