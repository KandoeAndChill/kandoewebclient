import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardratingComponent } from './cardrating.component';

describe('CardratingComponent', () => {
  let component: CardratingComponent;
  let fixture: ComponentFixture<CardratingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardratingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardratingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
