import {Injectable} from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import * as HTTP from 'http';


@Injectable()
export class ErrorInterceptor implements HttpInterceptor {


  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {


    return next.handle(request).pipe(catchError(err => {


        let errorMessageObject;


        try {

          console.log(err);

          // Authorization token is returned in the form of a String from the back-end...and so is the error response
          errorMessageObject = JSON.parse(err.error);


          return throwError(errorMessageObject.message);


        } catch (e) {

          // do nothing because request error is already an object
          errorMessageObject = err.error;

        }


        // Custom errors defined by back-end
        // When specific action is required in a component, we need to throw the whole object
        // With the status so the component can act accordingly.
        if (errorMessageObject.status) {

          return throwError(errorMessageObject);


        }




        // Else just send the errormessage...
        const error = errorMessageObject.message;
        console.log(error);
        return throwError(error);


      })
    );
  }
}
