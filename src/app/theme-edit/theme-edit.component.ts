import { Component, OnInit } from '@angular/core';
import { ThemeService} from '../service/theme.service';
import {Theme} from '../model/Theme';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';

@Component({
  selector: 'app-theme-edit',
  templateUrl: './theme-edit.component.html',
  styleUrls: ['./theme-edit.component.css']
})
export class ThemeEditComponent implements OnInit {
  editTheme: Theme;
  angForm: FormGroup;

  constructor(private themeService: ThemeService,
              private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    // this.editTheme = this.themeService.getActiveTheme();
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.themeService.getTheme(+params.get('id'))
        .subscribe((value: Theme) => {
        this.editTheme = value;
        });
    });
      this.angForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required, Validators.minLength(5)])],
      description: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      tag: new FormControl()
    });
  }

  updateTheme() {
    console.log('id: ' + this.editTheme.id);
    // this.themeService.setActiveTheme(this.editTheme);
    this.themeService.editTheme(this.editTheme)
      .subscribe((value) => {
        console.log(value);
        this.router.navigate(['/themes']);
      });
  }

}
