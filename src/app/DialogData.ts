import {CardDTO} from './model/CardDTO';

export interface DialogData {
  card: CardDTO;
}
