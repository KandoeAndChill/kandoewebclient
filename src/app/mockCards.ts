import { CardDTO } from './model/CardDTO';

const CARDS: CardDTO[] = [
  new CardDTO(),
  new CardDTO(),
  new CardDTO(),
  new CardDTO(),
  new CardDTO(),
  new CardDTO(),
  new CardDTO(),
  new CardDTO(),
  new CardDTO(),
  new CardDTO() ];

let i = 1;

CARDS.forEach(card => {
  card.setName('Card ' + i);
  i++;
});

export function getMockCards(): CardDTO[] {
  return CARDS;
}
