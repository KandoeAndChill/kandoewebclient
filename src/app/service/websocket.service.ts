import {Injectable} from '@angular/core';
import * as io from 'socket.io-client';
import {Observable} from 'rxjs';
import {Subject} from 'rxjs';
import {Card} from '../model/Card';
import {GameSessionDTO} from '../model/GameSessionDTO';
import {ChatMessage} from '../model/ChatMessage';


@Injectable()
export class WebsocketService {

  private socketbaseurl = 'http://localhost:5000/';

  // Our socket connection
  private chatsocketconnection;
  private sessionsocketconnection;

  constructor() {
  }

  connectChat(sessionid: number): Subject<MessageEvent> {

    this.chatsocketconnection = io(this.socketbaseurl + 'chat');

    this.joinChatRoom(sessionid);


    const observable = new Observable(observer => {
      this.chatsocketconnection.on('incomingmessage', (data) => {
        console.log('Received message from Websocket Server');
        console.log(data);
        observer.next(data);
      });
      return () => {
        this.chatsocketconnection.disconnect();
      };
    });


    const observer = {
      next: (message: ChatMessage) => {
        this.chatsocketconnection.emit('outcomingmessage', JSON.stringify(message));
      },
    };


    return Subject.create(observer, observable);
  }


  private joinChatRoom(sessionid: number) {

    this.chatsocketconnection.emit('joinchatroom', sessionid);

  }


  private joinSessionRoom(sessionid: number) {


    this.sessionsocketconnection.emit('joinsession', sessionid);

    console.log('joining session ' + sessionid);


  }

  connectSession(sessionid: number): Subject<MessageEvent> {

    this.sessionsocketconnection = io(this.socketbaseurl + 'session');


    this.joinSessionRoom(sessionid);


    const observable = new Observable(observer => {
      this.sessionsocketconnection.on('incomingsessionstate', (session) => {
        console.log('Received message from Websocket Server');
        console.log(session);

        observer.next(session);
      });
      return () => {
        this.sessionsocketconnection.disconnect();
      };
    });


    const observer = {
      next: (session: GameSessionDTO) => {
        console.log(session);
        this.sessionsocketconnection.emit('broadcastsession', { session: session, sessionId: session.id });
      },
    };


    return Subject.create(observer, observable);
  }


}
