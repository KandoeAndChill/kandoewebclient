import {Injectable, OnDestroy} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {map} from 'rxjs/operators';
import {WebsocketService} from './websocket.service';
import {ChatMessage} from '../model/ChatMessage';

@Injectable()
export class ChatService {


  chatmessages: Subject<any>;

  private chatSessionid;


  constructor(private http: HttpClient, private wsService: WebsocketService) {


  }

  setChatSessionId(sessionid: number) {

     this.chatSessionid = sessionid;
  }

  getMessages() {

    this.chatmessages = <Subject<any>>this.wsService
      .connectChat(this.chatSessionid).pipe(
        map((response: any): any => {
          return response;
        })
      );


    return this.chatmessages.asObservable();

  }


  sendMsg(message: ChatMessage) {

    this.chatmessages.next(message);


  }


}
