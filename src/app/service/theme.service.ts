import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Theme } from '../model/Theme';
import {THEMES} from '../mockThemes';
import {map} from 'rxjs/operators';
import {Observable, of, pipe} from 'rxjs';
import {EnvService} from './env.service';
import {SessionDTO} from '../model/SessionDTO';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private host = this.envService.apiUrl + 'themes/';

  results: Theme[] = [];
  activeTheme: Theme = new Theme();

  constructor(private http: HttpClient, private envService: EnvService) {

  }

  postTheme(theme: Theme) {
    return this.http.post(this.host + 'save', theme);
  }

  editTheme(theme: Theme) {
    return this.http.put(this.host + 'edit/' + theme.id, theme);
  }

  deleteTheme(theme: Theme) {
    return this.http.delete(this.host + 'delete/' + theme.id);
  }

  getThemesOfUser() {
    this.results = [];
    const promise = new Promise((resolve, reject) => {
      this.http
        .get(this.host + 'findAllByLoggedInUser')
        .toPromise()
        .then(
          (res: Theme[]) => {
            // Success
            res.forEach(theme => {
              this.results.push(theme);
            });
            resolve();
          },
          msg => {
            // Error
            reject(msg);
          }
        );
    });
    return promise;
  }

  getTheme(id: number) {
    return this.http.get(this.host + 'find/' + id + '/');
  }

  setActiveTheme(theme: Theme) {
    this.activeTheme = theme;
  }

  getActiveTheme() {
    return this.activeTheme;
  }
}
