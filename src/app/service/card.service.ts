import {Injectable} from '@angular/core';
import {CardDTO} from '../model/CardDTO';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {CardRatingDTO} from '../model/CardRatingDTO';
import {Rating} from '../model/Rating';
import {EnvService} from './env.service';

@Injectable({
  providedIn: 'root'
})
export class CardService {
  private host = this.envService.apiUrl + 'cards/';

  constructor(private http: HttpClient, private envService: EnvService) {
  }

  postCard(cardDTO: CardDTO): Observable<CardDTO> {
    return this.http.post(this.host + 'save', cardDTO)
      .pipe(map((response: any) => response),
        );
  }

  getCardsForSession(sessionId: number) {
    return this.http.get(this.host + 'findAllCards/' + sessionId + '/'
    );
  }

  getCardsForTheme(themeId: number): Observable<CardDTO[]> {
    return this.http.get(this.host + 'findAllCards/' + themeId + '/').
    pipe(map((response: any) => response));
  }

  getAllCardsByTheme(themaId: number): Observable<CardDTO[]> {

    return this.http.get(this.host + 'findAllCards/' + themaId).pipe(map((response: any) => response));

  }

  postCardRating(rating: CardRatingDTO): Observable<Rating[]> {

    return this.http.post(this.host + 'addCardRating', rating).pipe(map((response: any) => response));

  }

  getAllCardRatings(cardId: number): Observable<Rating[]> {

    return this.http.get(this.host + 'findAllRatingsByCard/' + cardId).pipe(map((response: any) => response));
  }

}
