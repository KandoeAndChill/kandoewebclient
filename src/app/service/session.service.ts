import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {SessionDTO} from '../model/SessionDTO';
import {map} from 'rxjs/operators';
import {SessionCreateDTO} from '../model/SessionCreateDTO';
import {Observable, Subject} from 'rxjs';
import {CardSessionDTO} from '../model/CardSessionDTO';
import {Card} from '../model/Card';
import {GameSessionDTO} from '../model/GameSessionDTO';
import {WebsocketService} from './websocket.service';
import {CardDTO} from '../model/CardDTO';
import {EnvService} from './env.service';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private sessionhost = this.envService.apiUrl + 'sessions/';
  private cardsessionhost = this.envService.apiUrl + 'cardSessions/';
  results: SessionDTO[] = [];
  currentSession: Subject<any>;

  constructor(private http: HttpClient, private wsService: WebsocketService, private envService: EnvService) {
  }

  getSessionsForUser() {
    this.results = [];
    const promise = new Promise((resolve, reject) => {
      this.http
        .get(this.sessionhost + 'findAllSessionsByLoggedInUser')
        .toPromise()
        .then(
          (res: SessionDTO[]) => {
            // Success
            res.forEach(session => {
              this.results.push(session);
            });
            resolve();
          },
          msg => {
            // Error
            reject(msg);
          }
        );
    });
    return promise;
  }

  getSessionsForTheme(id: number) {
    this.results = [];
    const promise = new Promise((resolve, reject) => {
      this.http
        .get(this.sessionhost + 'findAllSessions/' + id)
        .toPromise()
        .then(
          (res: SessionDTO[]) => {
            // Success
            res.forEach(session => {
              this.results.push(session);
            });
            resolve();
          },
          msg => {
            // Error
            reject(msg);
          }
        );
    });
    return promise;
  }

  getSessionsForOrganiser() {
    this.results = [];
    const promise = new Promise((resolve, reject) => {
      this.http
        .get(this.sessionhost + 'findAllSessionsByLoggedInOrganiser')
        .toPromise()
        .then(
          (res: SessionDTO[]) => {
            // Success
            res.forEach(session => {
              this.results.push(session);
            });
            resolve();
          },
          msg => {
            // Error
            reject(msg);
          }
        );
    });
    return promise;
  }

  getPassedSessionsForTheme() {
    // For Theme
    return this.http.get(this.sessionhost + 'findAllPassedSessions/');
  }

  getActiveSessionsForTheme() {
    // For Theme
    return this.http.get(this.sessionhost + 'findAllActiveSessions/');
  }

  getPlannedSessionsForTheme() {
    // For Theme
    return this.http.get(this.sessionhost + 'findAllPlannedSessions/');
  }

  createSession(session: SessionDTO): Observable<SessionDTO> {
    return this.http.post(this.sessionhost + 'save', session)
      .pipe(map((response: any) => response));
  }

  // gets the GameSessionDTO
  getGameSession(sessionId: number): Observable<GameSessionDTO> {
    return this.http.get(this.sessionhost + 'findGameSession/' + sessionId).pipe(map((response: any) => response));
  }

  inviteToSession(sessionId: number, userMailList: String[]): Observable<SessionDTO> {
    return this.http.put(this.sessionhost + 'inviteToSession', {sessionId: sessionId, mailList: userMailList})
      .pipe(map((response: any) => response));
  }

  getAllSessionsByTheme(themeId: number): Observable<SessionDTO[]> {
    return this.http.get(this.sessionhost + 'findAllSessions/' + themeId).pipe(map((response: any) => response));
  }

  cloneSession(sessionId: number) {
    return this.http.post(this.sessionhost + 'clone', sessionId);
  }

  deleteSession(sessionId: number) {
    console.log('deleting');
    return this.http.delete(this.sessionhost + 'delete/' + sessionId);
  }

  incrementCardSession(cardSessionId): Observable<GameSessionDTO> {
    console.log('incrementing cardsession' + cardSessionId);
    return this.http.put(this.sessionhost + 'playCard', cardSessionId).pipe(map((response: any) => response));
  }

  selectCardForSession(card: Card, sessionId: number): Observable<GameSessionDTO> {
    const cardDTO = new CardDTO();
    cardDTO.name = card.name;
    cardDTO.imageBase64Url = card.imageBase64Url;
    console.log(card);
    return this.http.post(this.sessionhost + 'selectCard/' + sessionId, cardDTO).pipe(map((response: any) => response));
  }

  endSelectionPhaseSession(sessionId: number): Observable<GameSessionDTO> {
    return this.http.post(this.sessionhost + 'endSelectionPhaseSession', sessionId).pipe(map((response: any) => response));
  }

  createSessionCards(cards: CardDTO[], sessionId: number): Observable<CardSessionDTO[]> {
    const cardSessionDTOs: CardSessionDTO[] = [];
    for (let i = 0; i < cards.length; i++) {
      cardSessionDTOs.push(this.convertCardToCardSessionDTO(cards[i], sessionId));
    }

    return this.http.post(this.cardsessionhost + 'saveAll/' + sessionId, cardSessionDTOs)
      .pipe(map((response: any) => response));
  }

  getSessionById(sessionId: number): Observable<SessionDTO> {
    return this.http.get(this.sessionhost + 'find/' + sessionId).pipe(map((response: any) => response));
  }

  getAllSessionCardsBySessionId(sessionId: number): Observable<CardSessionDTO[]> {
    return this.http.get(this.cardsessionhost + 'findAllCardSessionsBySession/' + sessionId).pipe(map((response: any) => response));
  }

  convertCardToCardSessionDTO(card: CardDTO, sessionId: number): CardSessionDTO {
    const cardSessionDTO = new CardSessionDTO();
    cardSessionDTO.cardText = card.name;
    cardSessionDTO.imageUrl = card.imageBase64Url;
    cardSessionDTO.sessionId = sessionId;
    return cardSessionDTO;
  }

  subscribeToSessionUpdate(sessionid: number) {
    this.currentSession = <Subject<any>>this.wsService
      .connectSession(sessionid).pipe(
        map((response: any): any => {
          return response;
        })
      );
    return this.currentSession.asObservable();
  }

  getSessionActiveUsernameByUserId(session: GameSessionDTO): Observable<String> {
    return this.http.get(this.sessionhost + 'findActiveUserNameById/' + session.activeUserId).pipe(map((response: any) => response));
  }

  broadcastSession(session: GameSessionDTO) {
    console.log(session);
    this.currentSession.next(session);
  }

  skipTurn(sessionId: number): Observable<GameSessionDTO> {
    return this.http.put(this.sessionhost + 'skipTurnCardSelection', sessionId).pipe(map((response: any) => response));
  }
}
