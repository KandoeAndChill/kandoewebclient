﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../model/User';
import {AlertService} from './alert.service';
import * as jwt_decode from 'jwt-decode';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {EnvService} from './env.service';

@Injectable()
export class UserService {
  private host = this.env.apiUrl + 'users/';
  loggedin;
  avatarOn;

  constructor(private env: EnvService, private http: HttpClient, private router: Router, private alertService: AlertService) {


    this.loggedin = sessionStorage.getItem('currentUser') ? true : false;
    this.avatarOn = sessionStorage.getItem('avatar') ? true : false;
  }

  login(userIdentifier: String, password: String) {
    /*this.http.post(this.host + 'signin', {userIdentifier, password}, {responseType: 'text'}).subscribe(
      token => {
        this.loggedin = true;
        this.alertService.success('logged in!');
        sessionStorage.setItem('currentUser', jwt_decode(token).sub);
        sessionStorage.setItem('token', token);
      },
      error => {
        this.alertService.error(error);
        this.loggedin = false;
      });

    return this.loggedin;*/
    const promise = new Promise((resolve, reject) => {
      this.http
        .post(this.host + 'signin', {userIdentifier, password}, {responseType: 'text'})
        .toPromise()
        .then(
          (token: any) => {
            // Success
            this.loggedin = true;
            this.alertService.success('logged in!');
            sessionStorage.setItem('currentUser', jwt_decode(token).sub);
            sessionStorage.setItem('token', token);
            this.http.get(this.host + 'me')
              .subscribe((value: User) => {
                this.setUserAvatar(value);
              });
            resolve();
          },
          msg => {
            // Error
            this.alertService.error(msg);
            this.loggedin = false;
          }
        );
    });
    return promise;
  }

  getAuthenticatedUser(): Observable<User> {
    return this.http.get(this.host + 'me').pipe(map((response: any) => response));
  }

  setUserAvatar(user: User) {
    if (user.avatarBase64 !== null) {
      console.log('Changing avatar');
      sessionStorage.setItem('avatar', user.avatarBase64);
      this.toggleAvatar(true);
      // location.reload(); // Om de image change direct te tonen
    }
  }

  updateUser(user: User) {
    return this.http.put(this.host + 'update', user);
  }

  toggleAvatar(bool: boolean) {
    this.avatarOn = bool;
  }

  getUsernameById(userId: number): Observable<String> {


    return this.http.get(this.host + 'findUserNameById/' +
      userId, {responseType: 'text'}).pipe(map((response: any) => response)).pipe(map((response: any) => response));


  }

  isOrganiserOfTheme(themeId: number): Observable<boolean> {

    return this.http.get(this.host + 'isThemeOrganiser/' + themeId).pipe(map((response: boolean) => response));


  }

  isOrganiserOfSession(sessionId: number): Observable<boolean> {

    return this.http.get(this.host + 'isSessionOrganiser/' + sessionId).pipe(map((response: boolean) => response));


  }

  logout() {
    sessionStorage.removeItem('currentUser');
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('avatar');
    this.loggedin = false;
    this.toggleAvatar(false);
  }

  getById(id: number) {
    return;
  }

  register(user: User) {
    return this.http.post(this.host + 'save', user, {responseType: 'text'});
  }

  update(user: User) {
    return;
  }

  delete(id: number) {
    return;
  }
}
