import {Component, DoCheck, ElementRef, OnInit, ViewChild} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {UserService} from '../service/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements DoCheck {

  @ViewChild('drawer')
  sideBar: ElementRef;

  userAvatar: string;


  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver, private  userService: UserService, private router: Router) {
  }

  ngDoCheck() {
    this.userAvatar = this.getUserAvatar();
  }


  currentUserAvailable(): boolean {
    return this.userService.loggedin;
  }

  getCurrentUser(): String {
    return sessionStorage.getItem('currentUser');
  }

  userAvatarAvailable(): boolean {
    return !!this.userService.avatarOn;
  }

  getUserAvatar(): string {
    return sessionStorage.getItem('avatar');
  }

  logout() {
    this.userService.logout();
    this.router.navigate(['login']);
  }
}
