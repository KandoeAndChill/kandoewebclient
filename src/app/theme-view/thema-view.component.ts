import {ChangeDetectorRef, Component, DoCheck, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {DomSanitizer} from '@angular/platform-browser';
import {ThemeService} from '../service/theme.service';
import {Router} from '@angular/router';
import {MatPaginator, MatSort, MatTableDataSource, MatTableModule} from '@angular/material';
import {Theme} from '../model/Theme';
import {THEMES} from '../mockThemes';
import {Observable} from 'rxjs';
import {UserService} from '../service/user.service';

@Component({
  selector: 'app-thema-view',
  templateUrl: './thema-view.component.html',
  styleUrls: ['./thema-view.component.css']
})
export class ThemaViewComponent implements OnInit, DoCheck {
  // sortedData: Theme[] = [];
  loader = true;
  error = false;
  themes: Theme[] = [];
  sortedData: Theme[];
  displayedColumns: string[] = ['name', 'description', 'tag', 'action'];
  @ViewChild('matSort') sort: MatSort;
  @ViewChild('paginator') paginator: MatPaginator;
  newTheme: Theme;

  // Attributen van het nieuwe Thema
  naam: string;
  beschrijving: string;
  tag: string[];
  userId: number;

  themeform: FormGroup;
  submitted = false;
  validated = false;

  closeResult: string;
  dataSource: MatTableDataSource<Theme>;
  updated: boolean;

  // voorlopig, om thema-delete te testen
  counter: number;

  constructor(private domSanitizer: DomSanitizer, private modalService: NgbModal, private formBuilder: FormBuilder,
              private themeService: ThemeService, private router: Router,
              private userService: UserService, private cdRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.getThemes();
    this.updated = false;
  }

  ngDoCheck(): void {
    if (this.updated === true) {
      this.getThemes();
    }
    this.updated = false;
  }

  getThemes() {
    this.themes = [];
    this.loader = true;
    this.themeService.getThemesOfUser().then(_ => {
      this.loader = false;
      this.error = false;
      this.themes = this.themeService.results;
      console.log(this.themes);
      this.dataSource = new MatTableDataSource<Theme>(this.themes);
      this.cdRef.detectChanges();
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, (err) => {
      this.loader = false;
      this.error = true;
      console.log(err);
      // TODO show error message
    });
  }

  onTheme(theme: Theme): void {
    this.router.navigate(['theme/', theme.id]);
  }

  get f() {
    return this.themeform.controls;
  }

  initializeThemeForm() {
    this.themeform = this.formBuilder.group({
      naam: ['', Validators.compose([Validators.required, Validators.minLength(5)])],
      beschrijving: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      Tag: new FormControl(),
      delete: new FormControl()
    });
  }

  open(content) {
    this.initializeThemeForm();
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.validated = false;
      this.submitted = false;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  postTheme() {
    if (this.themeform.invalid) {
      return;
    }
    this.newTheme = new Theme();
    this.newTheme.setDescription(this.beschrijving);
    this.newTheme.setName(this.naam);
    this.newTheme.setTag(this.tag);
    this.themeService.postTheme(this.newTheme)
      .subscribe((value: Theme) => {
        console.log(value);
        this.getThemes();
      });
  }

  editTheme(theme: Theme) {
    this.router.navigate(['theme/edit/' + theme.id]);
  }

  deleteTheme(theme: Theme) {
    this.themeService.deleteTheme(theme)
      .subscribe(
        () => this.updated = true
      );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
}


