import {Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {SessionComponent} from '../session/session.component';
import {CardDTO} from '../model/CardDTO';
import {ChatMessage} from '../model/ChatMessage';
import {checkAndUpdateTextDynamic} from '@angular/core/src/view/text';
import {ChatService} from '../service/chat.service';

const SockJs = require('sockjs-client');
const Stomp = require('stompjs');


@Component({
  selector: 'app-chat-form',
  templateUrl: './chat-form.component.html',
  styleUrls: ['./chat-form.component.css']
})
export class ChatFormComponent implements OnInit, OnDestroy {

  @Output()
  onMessageEvent: EventEmitter<ChatMessage> = new EventEmitter();

  message: String;

  chatMessage: ChatMessage;

  private stompClient;

  private serverUrl = 'http://localhost:8080/socket';

  messagesubscription;

  constructor(private chatService: ChatService) {
    this.message = '';


    this.messagesubscription = this.chatService.getMessages().subscribe((message) => {

        const chatMessage: ChatMessage = JSON.parse(message);


        this.onMessageCreate(chatMessage);


      }
    );

  }

  ngOnInit() {


  }

  ngOnDestroy() {

    this.messagesubscription.unsubscribe();


  }

  send(message: ChatMessage) {

    console.log(message);

    this.chatService.sendMsg(message);

  }

  handleSubmit(event) {
    if (event.keyCode === 13 || event instanceof MouseEvent) {
      if (this.message !== '') {
        this.chatMessage = new ChatMessage();
        this.chatMessage.origin = 'Saffet';
        this.chatMessage.content = this.message;
        console.log(this.chatMessage);
        this.onMessageCreate(this.chatMessage);
        this.send(this.chatMessage);
        this.message = '';
      }
    }
  }

  onMessageCreate(chatMessage: ChatMessage) {


    this.onMessageEvent.emit(chatMessage);
  }


}
