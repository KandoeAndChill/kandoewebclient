import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {CardCreatorComponent} from './card-view/card-creator.component';
import {CardOverviewComponent} from './card-overview/card-overview.component';
import {RegisterComponent} from './register/register.component';
import {LoginComponent} from './login/login.component';
import {SessionComponent} from './session/session.component';
import {ThemeOverviewComponent} from './theme-overview/theme-overview.component';
import {ChatComponent} from './chat/chat.component';
import {ThemaViewComponent} from './theme-view/thema-view.component';
import {MySessionsComponent} from './my-sessions/my-sessions.component';
import {HomePageComponent} from './home-page/home-page.component';
import {CardSelectionComponent} from './card-selection/card-selection.component';
import {CardratingComponent} from './cardrating/cardrating.component';
import {ThemeEditComponent} from './theme-edit/theme-edit.component';
import {UserEditComponent} from './user-edit/user-edit.component';

const appRoutes: Routes = [
  {path: '', component: HomePageComponent, pathMatch: 'full'},
  {path: 'register', component: RegisterComponent, pathMatch: 'full'},
  {path: 'login', component: LoginComponent, pathMatch: 'full'},
  {path: 'session/:sessionid', component: SessionComponent, pathMatch: 'full'},
  {path: 'sessions', component: MySessionsComponent, pathMatch: 'full'},
  {path: 'theme/:id/cards', component: CardOverviewComponent, pathMatch: 'full'},
  {path: 'card', component: CardCreatorComponent, pathMatch: 'full'},
  {path: 'theme', component: ThemeOverviewComponent, pathMatch: 'full'},
  {path: 'themes', component: ThemaViewComponent, pathMatch: 'full'},
  {path: 'chat', component: ChatComponent, pathMatch: 'full'},
  {path: 'session/:sessionid/select', component: CardSelectionComponent, pathMatch: 'full'},
  {path: 'rating', component: CardratingComponent, pathMatch: 'full'},
  {path: 'theme/:id', component: ThemeOverviewComponent, pathMatch: 'full'},
  {path: 'theme/edit/:id', component: ThemeEditComponent, pathMatch: 'full'},
  {path: 'theme/:id/sessions', component: MySessionsComponent, pathMatch: 'full'},
  {path: 'update', component: UserEditComponent, pathMatch: 'full'},
  {path: 'select/:sessionid', component: CardSelectionComponent, pathMatch: 'full'},
  {path: 'update/:id', component: UserEditComponent, pathMatch: 'full'}



];

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class AppRoutingModule {
}

export const routing = RouterModule.forRoot(appRoutes);
