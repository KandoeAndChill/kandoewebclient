import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  AfterViewChecked, OnDestroy
} from '@angular/core';
import {ResizedEvent} from 'angular-resize-event';
import {Card} from '../model/Card';
import {ChatMessage} from '../model/ChatMessage';
import {MatDialog} from '@angular/material';
import {MyDialogComponent} from '../my-dialog/my-dialog.component';
import {GameSessionDTO} from '../model/GameSessionDTO';
import {SessionService} from '../service/session.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CardSessionDTO} from '../model/CardSessionDTO';
import {ChatService} from '../service/chat.service';
import {CardService} from '../service/card.service';
import {Observable, timer} from 'rxjs';
import {CardDTO} from '../model/CardDTO';


@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.css']
})

export class SessionComponent implements OnInit, AfterViewChecked, OnDestroy {


  private nrOfCircles;
  private nrOfCirclesArray = [];
  private adjustedWidth;
  private adjustedHeight;
  private minwidth;
  private nrOfCards;
  private nrOfCardsArray = [];
  private increment;

  private cards: CardSessionDTO[] = [];
  colors: String[] = ['green', '#339FFF', 'orange', 'black', 'purple', 'pink', '#FFF033', 'grey'];

  cardfocusedId = -1;
  private navbarhidden = false;
  private cardPriorityMap: Map<string, number> = new Map();


  gameSession: GameSessionDTO;
  sessionsubscription;


  infomessage: String;
  errormessage: String;
  infotimercountdown: number;
  redirectdelay = 3;
  subscribeTimer: any;
  interval;


  // chat
  @ViewChild('navbar') private navBar: ElementRef;
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  @ViewChild('chatbox') private chatbox: ElementRef;
  disableScrollDown = false;
  chatMessages: ChatMessage[] = [];


  constructor(private chatService: ChatService,
              private cardService: CardService,
              private cdRef: ChangeDetectorRef,
              private router: Router,
              private route: ActivatedRoute,
              private sessionService: SessionService,
              private dialog: MatDialog) {


    let sessionId: number;

    this.route.paramMap.subscribe(params => {
      sessionId = parseInt(params.get('sessionid'), 10);
    });


    this.sessionService.getGameSession(sessionId).subscribe(gameSession => this.initializeGameSession(gameSession));

    this.scrollToBottom();
  }


  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  private scrollToBottom(): void {
    if (this.disableScrollDown) {
      return;
    }
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch (err) {
    }
  }

  private onScroll() {
    const element = this.myScrollContainer.nativeElement;
    const atBottom = element.scrollHeight - element.scrollTop === element.clientHeight;
    if (this.disableScrollDown && atBottom) {
      this.disableScrollDown = false;
    } else {
      this.disableScrollDown = true;
    }
  }


  ngOnInit() {


  }

  isChanceCircle() {
    return this.gameSession.chanceCircle;
  }

  initializeGameSession(gameSession: GameSessionDTO) {

    this.gameSession = gameSession;


    if (!this.gameSession.cardsSelected) {

      console.log(this.gameSession);
      this.infotimercountdown = this.redirectdelay;

      this.interval = setInterval(() => {
        if (this.infotimercountdown > 0) {
          this.infotimercountdown--;
        } else {

          this.redirectToCardSelection();
        }
      }, 1000);

    } else {
      if (gameSession.activeUserId === gameSession.organiserId) {
      this.sessionService.endSelectionPhaseSession(this.gameSession.id).subscribe(newGameSession => this.gameSession = newGameSession);

      // Create Default set of cardsessions from themecards if usercards are not enabled...setting Theme cards includes session refresh...
      this.cardService.getAllCardsByTheme(this.gameSession.themeId).subscribe(themecards => this.setSessionDefaultThemeCards(themecards));

      }
    }


    this.CardsAndDotsSetup(this.gameSession);


    this.subscribeToSessionBroadcastUpdate();
    this.setChatRoomIdBySessionId();

  }

  get redirectMessage(): String {

    if (this.infotimercountdown) {

      return 'No cards selected for session, redirecting to card selection page in...' + this.infotimercountdown;


    }
  }


  setSessionDefaultThemeCards(themeCards: CardDTO[]) {

    this.sessionService.createSessionCards(themeCards, this.gameSession.id).subscribe(sessionCards => {
      console.log('Sessioncards created from theme cards...' + sessionCards);
      this.OnSessionCardsUpdate();
    });


  }

  OnSessionCardsUpdate() {

    // Refresh Session.
    this.sessionService.getGameSession(this.gameSession.id).subscribe(gameSessionRefreshed => {
      this.gameSession = gameSessionRefreshed;
      this.CardsAndDotsSetup(gameSessionRefreshed);
    });


  }


  refreshSession() {

    // Refresh Session.
    this.sessionService.getGameSession(this.gameSession.id).subscribe(gameSessionRefreshed => this.gameSession = gameSessionRefreshed);


  }


  ngOnDestroy() {
    if (this.interval) {
      clearInterval(this.interval);
    }

    this.unsubscribeToSessionUpdate();
  }

  setChatRoomIdBySessionId() {


    this.chatService.setChatSessionId(this.gameSession.id);

  }


  CardsAndDotsSetup(session: GameSessionDTO) {


    console.log(session);

    this.cards = this.gameSession.cardList;

    // if first card start with 7, the id of the corresponding tag element in the UI will be 1..for convenience
    this.nrOfCircles = this.gameSession.numberOfCircles;
    this.minwidth = 15;
    this.increment = 15;


    this.nrOfCirclesArray = Array(this.nrOfCircles).fill(0).map((x, i) => i);

    if (this.cards.length > 0) {

      this.nrOfCards = this.cards.length;
      this.nrOfCardsArray = this.cards.map(card => card.id);

      for (let i = 0; i < this.cards.length; i++) {

        this.cardPriorityMap.set('card' + this.nrOfCardsArray[i], session.cardList[i].priority);

      }


    }


  }


  redirectToCardSelection() {

    this.router.navigate(['/select', this.gameSession.id]);
  }


  updateGameSessionCardsPriority(gameSession: GameSessionDTO) {


    this.gameSession = gameSession;

    console.log(gameSession);


    this.cards = gameSession.cardList;


    for (let i = 0; i < this.cards.length; i++) {

      this.cardPriorityMap.set('card' + this.nrOfCardsArray[i], gameSession.cardList[i].priority);

    }


  }


  initializeChat() {

    /*this.chatMessages = [

      {
        content: 'lol uw moeder',
        origin: 'Igor'
      },
      {
        content: 'lol uw moeder',
        origin: 'Saffet'
      },
      {
        content: 'lol uw moeder',
        origin: 'Igor'
      },
      {
        content: 'lol uw moeder',
        origin: 'Saffet'
      },
      {
        content: 'lol uw moeder',
        origin: 'Saffet'
      },
    ];*/


  }

  getCards(): CardSessionDTO[] {

    return this.cards;


  }

  unsubscribeToSessionUpdate() {

    this.sessionsubscription.unsubscribe();

  }

  subscribeToSessionBroadcastUpdate() {


    this.sessionsubscription = this.sessionService.subscribeToSessionUpdate(this.gameSession.id).subscribe(gameSession => {

      const gameSessionReturned: GameSessionDTO = JSON.parse(gameSession);

      console.log('session returned ' + gameSessionReturned);

      this.updateGameSessionCardsPriority(gameSessionReturned);


    });


  }

  getCardColor(cardnr): String {

    return this.colors[this.nrOfCardsArray.indexOf(cardnr)];


  }

  getCardPositionY(cardcirclenr, cardid) {

    return Math.sin(2 * Math.PI / this.nrOfCards * ((this.nrOfCardsArray.indexOf(cardcirclenr)))) *
      (this.adjustedWidth / 2 * ((this.nrOfCircles -
        (this.cardPriorityMap.get(cardid) ? this.cardPriorityMap.get(cardid) : 0)) / ((10 / (this.increment)) * 5)));
  }

  getCardPositionX(cardcirclenr, cardid) {
    return Math.cos(2 * Math.PI / this.nrOfCards * ((this.nrOfCardsArray.indexOf(cardcirclenr)))) *
      (this.adjustedWidth / 2 * ((this.nrOfCircles -
        (this.cardPriorityMap.get(cardid) ? this.cardPriorityMap.get(cardid) : 0)) / ((10 / (this.increment)) * 5)));
  }

  onCardClick() {
    /*
        if (this.gameSession.gameOver) {
          return;
        }*/


    const cardId = this.cardfocusedId;
    const currentPriorityCard = this.cardPriorityMap.get('card' + (cardId));


    this.sessionService.incrementCardSession(cardId).subscribe(gameSession => {
      this.sessionService.broadcastSession(gameSession);
      this.updateGameSessionCardsPriority(gameSession);
    });
    // this.cardPriorityMap.set('card' + (cardId), currentPriorityCard + 1);

    /* if (currentPriorityCard + 1 === this.nrOfCircles) {
       this.sessionactive = false;
       // this.openDialog(this.cards[cardId - 1].name);
     }*/
  }


  onResized(event: ResizedEvent) {
    this.adjustedWidth = event.newWidth;
    this.adjustedHeight = event.newHeight;
  }

  onCardHover(cardid: number) {


    this.cardfocusedId = cardid;

  }

  onCardMouseLeave() {
    this.cardfocusedId = -1;
  }

  // @HostListener('window:scroll', ['$event'])
  onScrollEvent($event) {
    if (window.pageYOffset > 64) {
      this.chatbox.nativeElement.style.top = 0;
      this.navbarhidden = true;

    } else {
      // this.chatbox.nativeElement.style.top = 100;
      this.navbarhidden = false;
    }
  }

  getChatBoxOffset(): number {
    return this.navbarhidden ? 0 : 7;
  }

  onMessageCreated(chatMessage: ChatMessage) {
    console.log(chatMessage);
    this.chatMessages.push(chatMessage);
    this.scrollToBottom();
  }

  openDialog(winner: String) {
    const dialogRef = this.dialog.open(MyDialogComponent, {
      width: '600px',
      data: 'Game is finished! The winner is ' + winner
    });
    // dialogRef.afterClosed().subscribe(result => {
    //   this.dialogResult = result;
    // });
  }

}
