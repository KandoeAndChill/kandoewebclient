import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {CardDTO} from '../model/CardDTO';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CardService} from '../service/card.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ThemeService } from '../service/theme.service';
import {Theme} from '../model/Theme';


@Component({
  selector: 'app-card-creator',
  templateUrl: './card-creator.component.html',
  styleUrls: ['./card-creator.component.css']
})
export class CardCreatorComponent implements OnInit {

  @Input() content;

  @Output()
  createcard: EventEmitter<CardDTO> = new EventEmitter();

  @Output()
  createcarderror: EventEmitter<CardDTO> = new EventEmitter();

  @ViewChild('fileInput')
  imageInputField: ElementRef;

  name = '';

  imageSrc: string;

  title: String;
  newCard: CardDTO;

  submitted = false;
  validated = false;
  imageboxchecked = false;

  cardform: FormGroup;
  closeResult: string;

  activeTheme: Theme;

  toggle(): void {
    this.imageboxchecked = !this.imageboxchecked;
  }

  constructor(private domSanitizer: DomSanitizer, private cardService: CardService, private modalService: NgbModal,
              private formBuilder: FormBuilder, private themeService: ThemeService) {
  }

  ngOnInit() {
    this.title = 'Kaart toevoegen';
    this.cardform = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(20)])]
    });
    this.activeTheme = this.themeService.getActiveTheme();
  }

  postCard() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.cardform.invalid) {
      return;
    }
    this.validated = true;
    this.newCard = new CardDTO();
    this.newCard.setName(this.name);
    this.newCard.setImageUrl(this.imageSrc);
    // TODO correct themeID meegeven
    this.newCard.setThemeId(this.themeService.activeTheme.id);

    this.cardService.postCard(this.newCard).subscribe(
      createdCard => this.onCardCreate(createdCard),
      error => this.onCardCreationError(error),
    )
    ;

    // this.createcard.emit(this.newCard);

  }

  get f() {
    return this.cardform.controls;
  }

  onCardCreate(createdCard: CardDTO) {
    console.log(createdCard);
    this.createcard.emit(createdCard);
  }

  onCardCreationError(error: any) {
    this.createcarderror.emit(error.message);
  }

  openInput() {
    this.imageInputField.nativeElement.click();
  }

  fileChange(files: File[]) {
    if (files.length > 0) {
      this.upload(files[0]);
    }
  }

  _handleReaderLoaded(e) {

    const reader = e.target;
    this.imageSrc = reader.result;

  }

  /**
   * this is used to perform the actual upload
   */
  upload(file: File) {
    const reader = new FileReader();
    console.log('upload');
    reader.onload = this._handleReaderLoaded.bind(this);
    const imageSrc = reader.readAsDataURL(file);
  }

  open(content) {

    this.resetForm();
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      // this.resetForm();
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.resetForm();

    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  private resetForm() {

    console.log('resetting');

    this.name = '';
    this.imageSrc = '';
    this.validated = false;
    this.submitted = false;
    this.imageboxchecked = false;

  }
}

