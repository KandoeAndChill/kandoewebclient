import {Theme} from './model/Theme';

const themes: Theme[] = [];
const theme1 = new Theme();
const theme2 = new Theme();
const theme3 = new Theme();

theme1.setName('Reizen');
theme2.setName('Uitgaan');
theme3.setName('Lunch');
theme1.setDescription('Waar gaan we op reis?');
theme2.setDescription('Waar gaan we op café?');
theme3.setDescription('Wat eten we voor lunch?');
theme1.setTag(['-']);
theme2.setTag(['Alcohol']);
theme3.setTag(['Frieten], [Hamburger], [Steak']);

themes.push(theme1);
themes.push(theme2);
themes.push(theme3);

export const THEMES: Theme[] = themes;


