import {Component, DoCheck, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {UserService} from '../service/user.service';
import {User} from '../model/User';
import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider, LinkedinLoginProvider
} from 'angular-6-social-login';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, DoCheck {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  completed = false;

  public userLoggedIn: User;
  userMade: boolean;
  newUser: User;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private userService: UserService,  private socialAuthService: AuthService) {
    this.userMade = false;
  }


  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(8)])]
    });
  }

  get f() {
    return this.loginForm.controls;
  }

  get user() {

    return sessionStorage.getItem('currentUser');

  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.loading = true;
    /*this.completed = this.userService.login(this.f.username.value, this.f.password.value);
    if (this.completed) {
      this.loading = false;
      this.router.navigate(['']);
    }*/
    this.userService.login(this.f.username.value, this.f.password.value);
  }

  ngDoCheck(): void {
    if (this.userService.loggedin && this.userService.avatarOn === false) {
      // De user service vullen
      this.userService.getAuthenticatedUser().subscribe((user) => {
        this.userService.setUserAvatar(user);
        this.router.navigate(['']);
      });
    }
  }

  public socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;
    if (socialPlatform === 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    } else if (socialPlatform === 'linkedin') {
      socialPlatformProvider = LinkedinLoginProvider.PROVIDER_ID;
    }

    this.socialAuthService.signIn(socialPlatformProvider).then(
      async (userData) => {
        console.log(socialPlatform + ' sign in data : ', userData);
        const token = userData.idToken;
        this.newUser = new User();
        this.newUser.setAvatar(userData.image);
        this.newUser.setEmail(userData.email);
        this.newUser.setUserName(userData.name);
        this.newUser.setPassword(token);
        // Waits until the user is registered to log them in
        await this.userService.register(this.newUser).toPromise().then( () => this.userService.login(userData.name, token), () =>
          this.userService.login(userData.name, token));
      }
    );
  }

}
