import {Theme} from '../model/Theme';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {ThemeService} from '../service/theme.service';
import {Component, DoCheck, OnInit, ViewChild} from '@angular/core';
import {CardCreatorComponent} from '../card-view/card-creator.component';

@Component({
  selector: 'app-theme-overview',
  templateUrl: './theme-overview.component.html',
  styleUrls: ['./theme-overview.component.css']
})
export class ThemeOverviewComponent implements OnInit, DoCheck {
  theme: Theme = new Theme();
  themeId: number;
  themeMade = false;

  constructor(private themeService: ThemeService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    // this.themeRedirected = this.themeService.getActiveTheme();
    this.route.paramMap.subscribe((params: ParamMap) => {
      console.log(params.get('id'));
      this.themeId = +params.get('id');
    }, (error) => {
      console.log(error);
    });
  }

  ngDoCheck(): void {
    if (this.themeMade === false) {
      console.log('themeMade = ' + this.themeMade);
      this.themeService.getTheme(this.themeId)
        .subscribe((value: Theme) => {
          this.theme = value;
          console.log(this.theme.name);
          this.themeService.setActiveTheme(this.theme);
        }, (err) => {
          console.log(err);
        });
      this.themeMade = true;
    }
  }

  onSessions() {
    console.log(this.theme.id);
    this.router.navigate(['/theme/', this.theme.id, 'sessions']);
  }
}
